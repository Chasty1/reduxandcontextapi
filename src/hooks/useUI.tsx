import { useSelector } from 'react-redux';
import { RootState } from '../store/configureStore';

function useUI() {
  const ui = useSelector((rootState: RootState) => rootState.uiReducer);

  return ui;
}

export default useUI;
