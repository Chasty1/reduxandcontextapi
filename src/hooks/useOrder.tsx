import { useDispatch, useSelector } from 'react-redux';
import { submitOrder } from '../redux/actions/order';
import { RootState } from '../store/configureStore';

function useOrder() {
  const dispatch = useDispatch();
  const order = useSelector((rootState: RootState) => rootState.orderReducer);
  const onSubmitOrder = (email: string) => dispatch(submitOrder(email));

  return { order, onSubmitOrder };
}

export default useOrder;
