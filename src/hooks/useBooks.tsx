import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getBooks, selectBook } from '../redux/actions/books';
import { RootState } from '../store/configureStore';

function useBooks() {
  const dispatch = useDispatch();
  const books = useSelector((rootState: RootState) => rootState.booksReducer);
  const selectCurrentBook = (bookId: string) => dispatch(selectBook(bookId));

  useEffect(() => {
    dispatch(getBooks());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { books, selectCurrentBook };
}

export default useBooks;
