import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from '../redux/reducers';
import createSagaMiddleware from 'redux-saga';
import saga from '../sagas';
import { booksMdl } from '../redux/middleware/books';
import { orderMdl } from '../redux/middleware/order';
import { api } from '../redux/middleware/api';

const initialiseSagaMiddleware = createSagaMiddleware();

const composeEnhancers = composeWithDevTools({
  // Specify here name, actionsBlacklist, actionsCreators and other options
});
const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(initialiseSagaMiddleware, ...booksMdl, ...orderMdl, api),
    // other store enhancers if any
  ),
);

export type RootState = ReturnType<typeof rootReducer>;

initialiseSagaMiddleware.run(saga);

export default store;
