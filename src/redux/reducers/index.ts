import { combineReducers } from 'redux';
import counter from './counter';
import count from './count';
import { booksReducer } from './books';
import { uiReducer } from './ui';
import { orderReducer } from './order';

const rootReducer = combineReducers({
  counter,
  count,
  booksReducer,
  uiReducer,
  orderReducer,
});

export default rootReducer;
