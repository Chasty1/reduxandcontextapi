const user = (state = {}, action: any) => {
  switch (action.type) {
    case 'DATA_LOADED':
      return action.payload;
    default:
      return state;
  }
};

export default user;
