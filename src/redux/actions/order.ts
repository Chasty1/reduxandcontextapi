export const CREATE_ORDER = '[order] Create';
export const UPDATE_ORDER = '[order] Update';
export const SUBMIT_ORDER = '[order] Complete order';

export const createOrder = (bookId: string) => ({
  type: CREATE_ORDER,
  payload: bookId,
});

export const updateOrder = (detalis: any) => ({
  type: UPDATE_ORDER,
  payload: detalis,
});

export const submitOrder = (email: string) => ({
  type: SUBMIT_ORDER,
  payload: email,
});
