import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import BooksScreen from '../../../screens/BooksScreen';
import OrderScreen from '../../../screens/OrderScreen';

const BookStack = createStackNavigator();

const BookStackNav = () => {
  return (
    <BookStack.Navigator>
      <BookStack.Screen name="Books" component={BooksScreen} />
      <BookStack.Screen name="Order" component={OrderScreen} />
    </BookStack.Navigator>
  );
};

export default BookStackNav;
