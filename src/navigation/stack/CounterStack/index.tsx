import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Counter from '../../../screens/CounterSagasScreen';

const CounterStack = createStackNavigator();

const CounterStackNav = () => {
  return (
    <CounterStack.Navigator>
      <CounterStack.Screen name="Counter" component={Counter} />
    </CounterStack.Navigator>
  );
};

export default CounterStackNav;
