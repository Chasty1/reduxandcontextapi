import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import BookStackNav from '../BookStack';
import HomeScreen from '../../../screens/HomeScreen';
import CounterStackNav from '../CounterStack';

const MainStack = createStackNavigator();

const MainStackNav = () => {
  return (
    <MainStack.Navigator headerMode="none">
      <MainStack.Screen name="Home" component={HomeScreen} />
      <MainStack.Screen name="Books Redux" component={BookStackNav} />
      <MainStack.Screen name="Counter Sagas" component={CounterStackNav} />
    </MainStack.Navigator>
  );
};

export default MainStackNav;
