import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store/configureStore';

const Counter = () => {
  const dispatch = useDispatch();
  const count = useSelector((rootState: RootState) => rootState.count);
  const incrementCount = () => {
    dispatch({ type: 'INCREMENT_COUNT' });
  };
  const decrementCount = () => {
    dispatch({ type: 'DECREMENT_COUNT' });
  };
  const incrementAsyncCount = () => {
    dispatch({ type: 'INCREMENT_ASYNC' });
  };

  return (
    <View style={styles.counter}>
      <Button onPress={() => incrementCount()} title="Increment" />
      <Button onPress={() => incrementAsyncCount()} title="Increment Async" />
      <Button onPress={() => decrementCount()} title="Decrement" />
      <Text>Count : {count}</Text>
    </View>
  );
};

export default Counter;

const styles = StyleSheet.create({
  counter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
