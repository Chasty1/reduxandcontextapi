import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Input } from 'react-native-elements';
import BookRow from '../components/BookRow';
import useOrder from '../hooks/useOrder';

const OrderScreen = ({ navigation, route }: any) => {
  const [email, setEmail] = useState('');
  const { id, thumbnail, title, description } = route.params;
  const { onSubmitOrder } = useOrder();

  return (
    <View style={styles.container}>
      <BookRow
        key={id}
        title={title}
        description={description}
        thumbnail={thumbnail}
      />

      <Input
        value={email}
        onChangeText={(value) => setEmail(value)}
        containerStyle={styles.input}
        placeholder="Enter your email"
      />
      <Button
        onPress={() => {
          onSubmitOrder(email);
          navigation.goBack();
        }}
        style={styles.button}
        title="Order Book Now"
      />
    </View>
  );
};

export default OrderScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  button: {
    width: '50%',
    alignSelf: 'center',
    marginTop: 16,
  },
  input: {
    width: 300,
  },
});
