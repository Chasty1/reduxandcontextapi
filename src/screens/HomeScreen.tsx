import React from 'react';
import {Pressable, StyleSheet, Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
import ExampleRow from '../components/ExampleRow';

const HomeScreen = ({navigation}: any) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerTitle}>Examples</Text>
      </View>
      <ExampleRow
        onPress={() => {
          navigation.navigate('Books Redux');
        }}
        icon="book-outline"
        title="Books Redux"
      />

      <ExampleRow
        onPress={() => {
          navigation.navigate('Counter Sagas');
        }}
        icon="timer-outline"
        title="Counter Sagas"
      />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  header: {
    height: 60,
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 18,
    fontWeight: '700',
  },
  container: {
    flex: 1,
    padding: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 10,
    alignItems: 'center',
  },
  icon: {
    width: 50,
    height: 50,
    backgroundColor: 'green',
  },
  title: {
    marginLeft: 10,
  },
});
