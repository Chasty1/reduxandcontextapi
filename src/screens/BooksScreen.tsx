import React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import BookRow from '../components/BookRow';
import Loading from '../components/Loading';
import useBooks from '../hooks/useBooks';
import useUI from '../hooks/useUI';

const NO_COVER =
  'https://books.google.com.pe/googlebooks/images/no_cover_thumb.gif';

const BooksScreen = ({ navigation }: any) => {
  const { books, selectCurrentBook } = useBooks();
  const ui = useUI();

  if (ui.pending) {
    return <Loading />;
  }

  return (
    <ScrollView>
      {books.map(({ volumeInfo, id }: any) => (
        <BookRow
          key={id}
          title={volumeInfo.title}
          description={volumeInfo.description || volumeInfo.subtitle}
          thumbnail={volumeInfo.imageLinks?.thumbnail || NO_COVER}
          onPress={() => {
            selectCurrentBook(id);
            navigation.navigate('Order', {
              id,
              thumbnail: volumeInfo.imageLinks?.thumbnail || NO_COVER,
              title: volumeInfo.title,
              description: volumeInfo.description || volumeInfo.subtitle,
            });
          }}
        />
      ))}
    </ScrollView>
  );
};

export default BooksScreen;
