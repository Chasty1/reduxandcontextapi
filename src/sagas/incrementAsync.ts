import { takeEvery, put, delay } from 'redux-saga/effects';

export function* incrementAsync() {
  yield delay(5000);
  yield put({ type: 'INCREMENT_COUNT' });
}

export function* watchIncrementAsync() {
  yield takeEvery('INCREMENT_ASYNC', incrementAsync);
}

//watch worker
