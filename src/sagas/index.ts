import { fork, all } from 'redux-saga/effects';
import loadData from './getData';
import { watchIncrementAsync } from './incrementAsync';

function* rootSaga() {
  //yield takeEvery('DATA_REQUESTED', workerSaga);
  yield all([fork(loadData), watchIncrementAsync()]);
}

export default rootSaga;
