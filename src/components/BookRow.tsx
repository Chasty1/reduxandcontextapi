import React from 'react';
import { Image, Pressable, StyleSheet, Text, View } from 'react-native';

type BookRowProps = {
  title: string;
  description: string;
  thumbnail: string;
  onPress?: () => void;
};

const DEFAULT_DESCRIPTION = 'This book does not contain any description.';

const BookRow = ({ title, description, thumbnail, onPress }: BookRowProps) => {
  return (
    <Pressable onPress={onPress} style={styles.container}>
      <Image
        resizeMode="contain"
        source={{ uri: thumbnail }}
        style={styles.bookImage}
      />
      <View style={styles.info}>
        <Text style={styles.title}>{title}</Text>
        <Text numberOfLines={3} style={styles.description}>
          {description || DEFAULT_DESCRIPTION}
        </Text>
      </View>
    </Pressable>
  );
};

export default BookRow;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    margin: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  bookImage: {
    height: 100,
    width: 125,
    aspectRatio: 2 / 3,
    borderRadius: 16,
    overflow: 'hidden',
  },
  info: {
    width: 0,
    flexGrow: 1,
    justifyContent: 'center',
    marginLeft: 16,
  },
  title: {
    fontSize: 26,
    fontWeight: 'bold',
  },
  description: {
    fontSize: 18,
    marginTop: 16,
  },
});
