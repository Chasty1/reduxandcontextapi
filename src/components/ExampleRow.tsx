import React from 'react';
import {Pressable, StyleSheet, Text} from 'react-native';
import {Icon} from 'react-native-elements';

type ExampleRowProps = {
  icon: string;
  title: string;
  onPress: () => void;
};

const ExampleRow = ({title, icon, onPress}: ExampleRowProps) => {
  return (
    <Pressable onPress={onPress} style={styles.row}>
      <Icon reverse name={icon} type="ionicon" color="#517fa4" />
      <Text style={styles.title}>{title}</Text>
      <Icon
        containerStyle={{position: 'absolute', right: 0, top: 15}}
        name="ios-chevron-forward"
        type="ionicon"
        color="#517fa4"
      />
    </Pressable>
  );
};

export default ExampleRow;

const styles = StyleSheet.create({
  header: {
    height: 60,
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 18,
    fontWeight: '700',
  },
  container: {
    flex: 1,
    padding: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 10,
    alignItems: 'center',
  },
  icon: {
    width: 50,
    height: 50,
    backgroundColor: 'green',
  },
  title: {
    marginLeft: 10,
  },
});
