import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import MainStackNav from './src/navigation/stack/MainStack';

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <NavigationContainer>
          <MainStackNav />
        </NavigationContainer>
      </SafeAreaView>
    </>
  );
};

/*const App = () => {
  const dispatch = useDispatch();
  const state = useSelector((rootState: RootState) => rootState.uiReducer);
  const getData = () => dispatch({type: 'DATA_REQUESTED'});
  useEffect(() => {
    //getData();
    dispatch(getBooks());
  }, []);

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        <Counter />
      </SafeAreaView>
    </>
  );
};*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
